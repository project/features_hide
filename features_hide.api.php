<?php
/**
 * @file
 * Hook provided by features_hide module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add features to the hide list.
 *
 * @return string|array
 *   The name of a feature to hide or an array of features to hide.
 */
function hook_features_hide() {
  return array(
    'feature_1',
    'feature_2',
  );
}

/**
 * Change the list of hidden features.
 *
 * This can be used to remove features from the array as well as adding them.
 * It is recommended that hook_feature_hide() is used to add features to the
 * array.
 *
 * @param array $hidden_features
 *   An array of features to be hidden.
 */
function hook_features_hide_alter(&$hidden_features) {
  while ($key = array_search('MYMODULE_feature', $hidden_features)) {
    unset($hidden_features[$key]);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
