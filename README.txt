Features Hide
-----
This is a simple development helper that hides example and test features
and test features that clutter up the UI.

Installation Instructions
-----
1. Download the module from drupal.org.
2. Copy to the sites/all/modules/contrib directory.
3. Enable the module through the UI by navigating to admin/modules.

Supported Modules
----
Currently, the following modules have their test/example features hidden:
entityform
features_extra
feeds
flexslider
migrate
migrate_extra
uuid

Hiding Additional Features
----
Developers can hide additional features from the UI by implementing one
of two provided hooks:

hook_features_hide() - Can be implemented to add additional features
to the list of hidden features.

hook_features_hide_alter() - Allows for altering the hide list by adding
or removing features.

See features_hide.api.php for more information.
